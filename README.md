`SWATmeasR` moved to GitHub. All versions > 0.8.2 are available from there. <img src="man/figures/octomove.PNG" align="right" width="250" />

You can install `SWATmeasR` from the GitHub repository as follows:

```r
remotes::install_github('chrisschuerz/SWATmeasR')
```

If you need any version older than 0.8.2 they are available from this repository and can be installed with:


```r
remotes::install_git('https://git.ufz.de/schuerz/swatmeasr.git')
```